# BloguisteCMS

**BloguisteCMS** est un CMS spécialisé dans la création de blog.

## Prérequis

### 1. Pour l'installation

- Apache => 2.4
- PHP => 7.2
- MySQL => 5.7

### 2. Pour le développement

- npm => 3.5

## Fonctionnalités

- Panel d'administration
- Custom PHP framework
- w3css
- jQuery

## Installation

1. Téléchargez le dépôt officiel du projet [Bitbucket](https://bitbucket.org/eliseekn/bloguistecms) et copiez le dans votre répertoire localhost
2. Procédez à l'installation du CMS en allant sur ***localhost/bloguistecms/install***
3. Enjoy it!

## Contribution

Toute contribution au projet telle qu'elle soit est la bienvenue.

## Licence

[MIT](https://choosealicense.com/licenses/mit/)
